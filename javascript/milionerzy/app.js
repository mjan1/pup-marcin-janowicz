var questions = [
    {
    'question': 'Ile Hubert ma pytań',
    'answers' : ['6', '16', '26', '73'],
    'correctAnswer' : '16'
},
    {
        'question': "jak na imie Monika Brodka",
        'answers': ['Monika', 'Anna', 'Janusz', 'Zofia'],
        'correctAnswer' : 'Janusz'       
    },
    {
        'question': 'Ktora jest godzina?',
       'answers': ['w pol do komina','14','28','45'],
       'correctAnswer': '14'
        
        }
];

var usedIndexes = [];
var currentQuestionNum = selectQuestionNum(usedIndexes);
var currentQuestion = questions[currentQuestionNum];


function selectQuestionNum () {
    var q = Math.floor(Math.random() * questions.length);
    if(usedIndexes.length < questions.length) {
        while(usedIndexes.indexOf(q) >=0) {
            q = Math.floor(Math.random() * questions.length);
        }
        usedIndexes.push(q);
    } else {
        q = -1;
    }
    return q;
    
}currentQuestionNum

function fillData(question) {
    var questionBox = document.getElementById('question');
    var answersElems = document.querySelectorAll('button');
    questionBox.innerHTML = question.question;
    for (var i = 0; i < answersElems.length; i++) {
        answersElems[i].innerHTML = question.answers[i];
    }
}

function addListeners () {
   var btns = document.querySelectorAll('button');
   for (var i = 0; i < btns.length; i++) {
       btns[i].addEventListener('click', function(e){
           if (e.target.innerHTML == currentQuestion.correctAnswer){
            currentQuestionNum = selectQuestionNum();
            currentQuestion = questions[currentQuestionNum];
        fillData(currentQuestion);
                     
     
           } else {
              alert('KONIEC GRY');
       
           }
       });
   }
}

addListeners();
fillData(currentQuestion);
